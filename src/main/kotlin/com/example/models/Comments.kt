package com.example.models

import kotlinx.serialization.Serializable

@Serializable
data class Comments(
    val id:Int,
    val comment:String,
    val date:String
)