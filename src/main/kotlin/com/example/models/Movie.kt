package com.example.models

import kotlinx.serialization.Serializable

@Serializable
data class Movie(
    val id:Int,
    var title:String,
    var genre:String,
    var director:String,
    val comments:MutableList<Comments>
)
val filmStorage= mutableListOf<Movie>()