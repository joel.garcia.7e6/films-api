package com.example.routes

import com.example.models.Comments
import com.example.models.Movie
import com.example.models.filmStorage
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

fun Route.filmsRouting(){
    route("/films"){
        get{
            if(filmStorage.isNotEmpty()) call.respond(filmStorage)
            else call.respondText("No films found.", status = HttpStatusCode.OK )
        }
        get("{id?}"){
            if (call.parameters["id"].isNullOrBlank()) return@get call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            val id = call.parameters["id"]?.toInt()
            for (film in filmStorage) {
                if (film.id == id) return@get call.respond(film)
            }
            call.respondText(
                "Movie with id $id not found",
                status = HttpStatusCode.NotFound
            )
        }
        post {
            val movie = call.receive<Movie>()
            filmStorage.add(movie)
            call.respondText("Movie stored correctly", status = HttpStatusCode.Created)
        }
        put("{id?}") {
            if (call.parameters["id"].isNullOrBlank()) return@put call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            val id = call.parameters["id"]?.toInt()
            val filmToUpd = call.receive<Movie>()
            for (film in filmStorage) {
                if (film.id == id) {
                    film.title = filmToUpd.title
                    film.genre = filmToUpd.genre
                    film.director = filmToUpd.director
                    return@put call.respondText(
                        "Movie with id $id has been updated",
                        status = HttpStatusCode.Accepted
                    )
                }
            }
            call.respondText(
                "Movie with id $id not found",
                status = HttpStatusCode.NotFound
            )
        }
        delete("{id?}") {
            if (call.parameters["id"].isNullOrBlank()) return@delete call.respondText(
                "Missing id",
                status = HttpStatusCode.BadRequest
            )
            val id = call.parameters["id"]?.toInt()
            for (film in filmStorage) {
                if (film.id == id) {
                    filmStorage.remove(film)
                    return@delete call.respondText("Movie removed correctly", status = HttpStatusCode.Accepted)
                }
            }
            call.respondText(
                "Movie with id $id not found",
                status = HttpStatusCode.NotFound
            )
        }
        route("/{id?}/comments"){
                post {
                    val postComment = call.receive<Comments>()
                    if (call.parameters["id"].isNullOrBlank()) {
                        return@post call.respondText("Missing id", status = HttpStatusCode.BadRequest)
                    }
                    val id = call.parameters["id"]?.toInt()
                    for (i in filmStorage.indices){
                        if (filmStorage[i].id == id){
                            filmStorage[i].comments.add(postComment)
                        }
                    }
                    call.respondText("Review stored correctly", status = HttpStatusCode.Created)
                }
                get {
                    if (call.parameters["id"].isNullOrBlank()) {
                        return@get call.respondText("Missing id", status = HttpStatusCode.BadRequest)
                    }
                    val id = call.parameters["id"]?.toInt()
                    for (i in filmStorage.indices){
                        if (filmStorage[i].id == id){
                            for (j in filmStorage[i].comments){
                                return@get call.respond(j)
                            }
                            call.respondText("Comment not found", status = HttpStatusCode.NotFound)
                        }
                    }
                    call.respondText("Movie with id $id not found.", status = HttpStatusCode.NotFound)
                }
            }


        }
    }